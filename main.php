<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:17
 */

include "utils.php";

$back = "<";
$weekNummer = -1;
$dagNummer = -1;
selectWeek();

function selectWeek()
{
    while (TRUE) {
        $week = getInput("In welke week wil je werken?:");
        if (!file_exists("./week-" . $week . "/")) {
            $dir = new DirectoryIterator("./");
            print("[!] Ongeldig week nummer. Kies uit:\n");
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                    if (strncmp($fileinfo->getFilename(), "week", strlen("week")) === 0) {
                        print (" - " . $fileinfo->getFilename() . "\n");
                    }
                }
            }
        } else {
            global $weekNummer;
            $weekNummer = $week;
            selectDay($week);
            break;
        }
    }

}

function selectDay($weekNummer)
{
    global $back;
    while (TRUE) {
        $dag = getInput("Welke dag wil je gebruiken?:");
        if($dag == $back) {
            selectWeek();
        }
        if (!file_exists("./week-" . $weekNummer . "/dag-" . $dag . ".php")) {
            $dir = new DirectoryIterator("./week-" . $weekNummer);
            print("[!] Ongeldig dag nummer. Kies uit:\n");
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isFile() && !$fileinfo->isDot()) {
                    if (strncmp($fileinfo->getFilename(), "dag", strlen("dag")) === 0) {
                        print (" - " . $fileinfo->getFilename() . "\n");
                    }
                }
            }
        } else {
            require "./week-" . $weekNummer . "/dag-" . $dag . ".php";
            if (file_exists("./week-" . $weekNummer . "/dag-" . $dag . ".php")) {
                include_once './week-' . $weekNummer . '/dag-' . $dag . '.php';
                if (class_exists("opdrachten")) {
                    global $dagNummer;
                    $dagNummer = $dag;
                    
                    selectOpdracht(true);
                    break;
                } else {
                    printLine("[!] Deze dag heeft geen opdrachten class, daarom kan deze class niet gerunt worden!");
                }
            }
        }
    }
}

function selectOpdracht($firstTime)
{
    global $back;
    global $weekNummer;

    while (TRUE) {
        if(!$firstTime)
            print ("\n \n \n \n \n");
        $opdrachtNummer = getInput("Welke opdracht wil je " . ($firstTime ? "" : "nu ") ."opstarten?:");
        if($opdrachtNummer == $back) {
            selectDay($weekNummer);
        }
        if (!method_exists("opdrachten", "opdracht" . $opdrachtNummer)) {
            if(strtolower($opdrachtNummer) == 'stop') {
                print ("Shutting down...");
                break;
            }
            print("Geef een geldig opdracht nummer op:\n");
            $functions = get_class_methods("opdrachten");
            foreach ($functions as $key => $value) {
                print (" > " . $value . "\n");
            }
        } else {
            $opdrachten = new opdrachten();
            print ("\n \n \n \n \n");
            call_user_func(array($opdrachten, 'opdracht' . $opdrachtNummer));
            selectOpdracht(false);
            break;
        }
    }
}