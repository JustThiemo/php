<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:17
 */

function printLine($message) {
    print($message . "\n");
}

function getInput($message)
{
    print(">> " . $message . " ");
    return trim(fgets(STDIN));
}

function getQuestionInput($message)
{
    print(">> " . $message . " ");
    $yes = ["y", "yes", "j", "ja"];

    $output = strtolower(trim(fgets(STDIN)));
    if ($output != null) {
        if (in_array($output, $yes)) {
            return true;
        }
    }
    return false;

}