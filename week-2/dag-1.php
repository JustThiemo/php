<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:42
 */

class opdrachten
{

    function opdracht1()
    {
        $klassenlijst = array("tobias", "hasna", "aukje", "fred");
        print_r($klassenlijst);

        for ($int = 0; $int < count($klassenlijst); $int++) {
            print($int . ": " . $klassenlijst[$int] . "\n");
        }

        asort($klassenlijst);
        print_r($klassenlijst);
    }

    function opdracht2()
    {
        $songs = array();
        $songsAmount = 3;
        $songsDone = 0;
        while ($songsDone < $songsAmount) {
            print("\nSong voor index " . $songsDone . ": ");
            $songs[$songsDone] = trim(fgets(STDIN));
            $songsDone++;
        }
        print_r($songs);
    }

    function opdracht3()
    {
        $scores = array(5, 5, 6, 6, 7);
        foreach ($scores as $index => $score) {
            ("Score " . ($index + 1) . ": " . $score);
        }

        printLine("Aantal rondes: " . count($scores));
        printLine("Totale tijd: " . array_sum($scores));
        printLine("Snelste tijd: " . min($scores));
        printLine("Sloomste tijd: " . max($scores));
    }

    function opdracht4()
    {
        $klassenlijst = array("tobias", "hasna", "aukje", "fred",
            "sep", "koen", "wahed", "anna", "jackie", "rashida",
            "winston", "sammy", "manon", "ben", "karim", "bart",
            "lisa");
        foreach ($klassenlijst as $index => $leerling) {
            printLine(($index + 1) . ". " . ucfirst($leerling));
        }
    }

    function opdracht5()
    {
        $nummertjes = array(1, 8, 12, 7, 14, 14, -13, 8, 1, -1, 14, 7, 15);
        print_r($nummertjes);

        $max = max($nummertjes);
        printLine("Max: " . $max);
        printLine("Totaal: " . array_sum($nummertjes));
        $aantal = 0;
        foreach ($nummertjes as $nummertje) {
            if ($nummertje < 0)
                $nummertje = $nummertje * -1;
            $aantal += $nummertje;
        }
        printLine("Totaal: " . $aantal);

        $hoogste = 0;
        $gevonden = 0;
        foreach ($nummertjes as $nummertje) {
            if ($nummertje < $hoogste)
                continue;

            if ($nummertje > $hoogste) {
                $hoogste = $nummertje;
                $gevonden = 1;
            } else if ($hoogste == $nummertje) {
                $gevonden++;
            }
        }
        printLine("Hoogste gevond: " . $hoogste . ", kwam " . $gevonden . "x voor.");
    }

    function opdracht6()
    {
        $telefoonNummer = array(
            "Mickey Mouse" => "038-4699776",
            "Guus Geluk" => "0578-121212",
            "Donald Duck" => "010-2311512",
        );

        foreach ($telefoonNummer as $naam => $telnr) {
            printLine($naam . ": " . $telnr);
        }
        printLine("Wie krijgt er een nieuw telefoonnummer?: ");
        $nieuwe = trim(fgets(STDIN));
        if (array_key_exists($nieuwe, $telefoonNummer)) {
            printLine("Wat is het nieuwe nummer?: ");
            $nieuwTelNr = trim(fgets(STDIN));
            $telefoonNummer[$nieuwe] = $nieuwTelNr;
        } else {
            printLine("Deze persoon zit niet in het telefoonboek.");
        }

        printLine("Nieuwe gegevens:");
        foreach ($telefoonNummer as $naam => $telnr) {
            printLine($naam . ": " . $telnr);
        }
    }

    function opdracht7() {
        $prijzen = array(
            "cola" => 2.50,
            "koffie" => 2,
            "thee" => 1.75,
            "bier" => 2.25,
            "wijn" => 3.75,
            "water" => 0.50
        );

        $hoogstePrijs = 0;
        $hoogste = "";
        $totalePrijs = 0;
        $aantalProducten = 0;

        foreach ($prijzen as $product => $prijs) {
            if ($product == 'thee') {
                $prijzen[$product] = ($prijs + 0.10);
            } else if ($product == 'water') {
                $prijzen[$product] = ($prijs * 2);
            }
        }

        foreach ($prijzen as $product => $prijs) {
            if ($prijs > $hoogstePrijs) {
                $hoogste = ucfirst($product) . " €" . $prijs;
                $hoogstePrijs = $prijs;
            }
            $totalePrijs += $prijs;
            $aantalProducten++;
        }
        printLine("Hoogste prijs: " . $hoogste);
        printLine("Gemiddelde prijs: €" . ($totalePrijs / $aantalProducten));

        $bestelling = array(
            "cola", "bier", "cola", "wijn", "water", "koffie", "koffie", "koffie"
        );

        $prijs = 0;
        foreach($bestelling as $product) {
            $prijs += $prijzen[$product];
        }

        $bestelling2 = array(
            "cola" => 2,
            "bier" => 1,
            "wijn" => 1,
            "water" => 1,
            "koffie" => 3
        );

        $prijs = 0;
        foreach($bestelling2 as $product => $aantal) {
            $prijs += ($prijzen[$product] * $aantal);
        }
    }
}
