<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:48
 */

class opdrachten {
    function opdracht8()
    {

        $firstTime = true;
        while (true) {
            if (!$firstTime) {
                $inp = getQuestionInput("Wil je doorgaan? ja/nee:");
                if (!$inp) {
                    break;
                }
            }

            $firstTime = false;
            $input = getInput("Moet het een driehoek worden of een vierkant/rechthoek?:");
            $driehoek = strtolower($input) == 'driehoek';

            if ($driehoek) {
                $ster = "";
                $totAantal = getInput("Hoe lang moet het driehoekje worden?:");
                if (is_numeric($totAantal)) {
                    $mirror = getQuestionInput("Moet de driehoek gespiegeld worden?:");
                    if ($mirror) {
                        drawTriangleMirrored($totAantal);
                    } else {
                        drawTriangle($totAantal);
                    }
                } else {
                    print("[!] Je hebt geen geldige lengte opgegeven.\n");
                }
            } else {
                $x = getInput("Hoeveel sterren per één rij?:");
                $y = getInput("Hoeveel rijen?:");

                if (!is_numeric($x) || !is_numeric($y)) {
                    print("[!] Je hebt geen geldige x en/of y waarde opgegeven.\n");
                } else {
                    drawBox($x, $y);
                }
            }

        }
    }
}

function drawBox($x, $y)
{
    for ($x2 = 0; $x2 < $x; $x2++) {
        $ster = "";
        for ($y2 = 0; $y2 < $y; $y2++) {
            $ster = $ster . "*";
        }
        print($ster . "\n");
    }
}

function drawTriangleMirrored($amount)
{
    $draw = true;
    $ster = "";
    $up = true;

    while ($draw) {
        if ($up) {
            for ($i = 1; $i <= $amount; $i++) {
                $ster = $ster . "*";
                print($ster . "\n");
            }
            $up = false;
        } else {
            for ($i = $amount; $i > 0; $i--) {
                $ster = substr($ster, 0, ($i - 1));
                print($ster . "\n");
            }
            $draw = false;
        }
    }
}

function drawTriangle($amount)
{
    $ster = "";
    for ($i = 1; $i <= $amount; $i++) {
        $ster = $ster . "*";
        print($ster . "\n");
    }
}