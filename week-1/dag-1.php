<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:28
 */

class opdrachten {

    function opdracht1() {
        print "Hello World! ";
        print ("\n");

        print "Hoe heet je? ";
        $naam = trim(fgets(STDIN));
        print "Hallo, ".$naam . "\n";

        $stripfiguur1 = "Donald";
        $stripfiguur2 = "Jerry";
        print "Favoriete stripfiguren: " . $stripfiguur1 ." en " . $stripfiguur2;

    }

    function opdracht2() {
        $voorNaam = "Alwin de Vries";
        $straatNaam = "Bomenlaan";
        $huisNummer = 12;
        $adres = $straatNaam . " " . $huisNummer;

        print $voorNaam . ", " . $adres;
    }

    function opdracht3() {
        print "Geef 2 getallen op: ";
        $getal1 = trim(fgets(STDIN));
        $getal2 = trim(fgets(STDIN));
        print "Getal 1: " . $getal1 . " en Getal 2: " . $getal2;

        print "\n";
        $som = (int) $getal1 + (int) $getal2;
        print $getal1 . "+" . $getal2 . "=" . $som;
    }

    function opdracht4() {
        $array = [
            "Pizza" => "8.50",
            "Macaroni" => "5.25",
            "Lasagna" => "5.95",
        ];

        print "Groter multiplier: ";
        $groter = trim(fgets(STDIN));

        print "Menukaart:\n";

        foreach($array as $key => $value) {
            print $key . ", Klein:" . $value . " Groot: " . ($value * $groter) . "\n";
        }
    }

    function opdracht5() {
        $a = 2;
        for($i = 0; $i <= 2; $i++) {
            print($a);
            $a = ($a * 2);
        }

        print "\n";

        $s = "aa";
        $up = true;

        $ignoredList = ["a", "b", "c"];

        foreach ($ignoredList as $ignore) {
            $output = $s;
            if($up)
                $output = strtoupper($output);
            $up = !$up;
            print $output . " " . strlen($output) . "\n";
            $s = $s . $s;
        }
    }
}