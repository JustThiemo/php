<?php
/**
 * Created by PhpStorm.
 * User: Thiemo
 * Date: 28-9-2018
 * Time: 11:56
 */

/**
 * Trim input and give a pre-set message for the input.
 * @param $message - Message before input
 * @return string - Returns the input
 */
function trimit($message)
{
    print("\n» " . $message);
    return trim(fgets(STDIN));
}

/**
 * Trim input and give a pre-set message for the input.
 * Output the given string with the formatted value
 * @param $message - Message before input
 * @param $output - Message for output, put %s to be replaced with the input
 * @return string - Returns the message formatted
 */
function trimprint($message, $output)
{
    print("\n» " . $message);
    $value = trim(fgets(STDIN));
    print(str_replace("%s", $value, $output));
    return $value;
}

class opdrachten
{

    function opdracht11()
    {
        $aantal = trimprint("Voer aantal vogels in: ", "Ik zie %s vogels");
    }

    function opdracht12()
    {
        $persoon = strtolower(trimit("Voer in; ik, jij, hij:"));
        $verjaardag = "14 Januari";

        if ($persoon == 'ik') {
            print ("Ik ben ");
        } else if ($persoon == 'jij') {
            print("Jij bent ");
        } else if ($persoon == 'hij') {
            print ("Hij is ");
        } else {
            print ("Niet geldige invoer.");
        }

        print (" jarig op " . $verjaardag);
    }

    function opdracht13()
    {
        $leeftijd = trimit("Hou oud ben je?: ");
        $message = "Je mag ";
        if ($leeftijd < 18) {
            $message = $message . "nog niet ";
        }
        $message = $message . "stemmen";
        print($message);
    }

    function opdracht14()
    {
        $tempratuur = trimit("Hoe warm wordt het?: ");
        if ($tempratuur > 25) {
            print ("We gaan naar het strand");
        } else {
            print ("We blijven thuis");
        }

    }

    function opdracht15()
    {
        $anna = trimit("Hou oud is Anna?: ");
        $bob = trimit("Hou oud is Bob?: ");
        if (is_numeric($anna) && is_numeric($bob)) {
            if ($anna == $bob)
                print ("Anna is even oud als Bob.");
            else
                print ("Anna is " . (($anna > $bob) ? "ouder dan" : "jonger dan") . " Bob.");
        } else {
            print("\nEen van de leeftijden is niet correct ingevoerd.");
        }
    }

    function opdracht16()
    {
        $sjors = trimit("Hoeveel boterhammen eet Sjors?: ");
        $mathilde = trimit("Hoeveel boterhammen eet Mathilde?: ");
        $sharon = trimit("Hoeveel boterhammen eet Sharon?: ");

        function getMessage($amount, $gemiddeld)
        {
            if ($amount == $gemiddeld)
                return "dat is gemiddeld";
            return ($amount > $gemiddeld ? "dat zijn er behoorlijk veel" : "dat is best weinig");
        }

        if (is_numeric($sjors) && is_numeric($mathilde) && is_numeric($sharon)) {
            $gemiddeld = ($sjors + $mathilde + $sharon) / 3;
            print ("Gemiddeld gegeten: " . $gemiddeld);
            print ("\nSjors eet " . $sjors . " boterhammen, " . getMessage($sjors, $gemiddeld));
            print ("\nMathilde eet " . $mathilde . " boterhammen, " . getMessage($mathilde, $gemiddeld));
            print ("\nSharon eet " . $sharon . " boterhammen, " . getMessage($sharon, $gemiddeld));
        } else {
            print ("\nEen van de aantallen is niet correct ingevoerd.");
        }
    }

    function opdracht17()
    {
        $stelling1 = "Olifant schrijf je met een O";
        //de substring functie kort het in, dus in dit geval de eerste letter van de stelling.
        $waarheid1 = substr(strtolower($stelling1), 0, 1) == 'o';
        print ("\nStelling 1 (" . $stelling1 . ") is ");
        print ($waarheid1 ? "WAAR" : "NIET WAAR");

        $stelling2 = "Panda schrijf je met een M";
        $waarheid2 = substr(strtolower($stelling2), 0, 1) == 'm';
        print ("\nStelling 2 (" . $stelling2 . ") is ");
        print ($waarheid2 ? "WAAR" : "NIET WAAR");
    }

    function opdracht18()
    {
        $regenInput = trimit("Regent het vandaag? (ja/nee): ");
        $regen = strtolower($regenInput) == 'ja';
        $meerijdenInput = trimit("Kan je met iemand meerijden? (ja/nee):");
        $meerijden = strtolower($meerijdenInput == 'ja');
        if ($regen && $meerijden) {
            print ("Het regent, gelukkig kan ik vandaag met iemand meerijden");
        } else {
            print ("Ik ga op de fiets vandaag. " . ($regen ? "Dit vind ik super kut want het regent." : "Dit maakt me niet uit, het is lekker weer"));
        }
    }

    function opdracht19()
    {
        $prijsPerKM = 0.1;
        $afstand = 10;

        /**
         * @param $weekend - Is het weekend?
         * @param $gratisInWeekend - Reis je gratis in het weekend?
         * @return float|int - De prijs
         */
        function adjustPrice($weekend, $gratisInWeekend)
        {
            global $afstand;
            global $prijsPerKM;

            if ($weekend) {
                if (!$gratisInWeekend)
                    return (($prijsPerKM * $afstand) / 0.60);
            } else {
                if ($gratisInWeekend)
                    return (($prijsPerKM * $afstand) / 0.60);
            }
            return 0;
        }

        $doordeweeks = [
            "maandag",
            "dinsdag",
            "woensdag",
            "donderdag",
            "vrijdag"
        ];

        $weekend = [
            "zaterdag",
            "zondag"
        ];

        $afstandInput = trimit("\n\n-*- Treinkaarten -*-\nHoe veel kilometer reis je?: ");
        if (is_numeric($afstandInput)) {
            $afstand = (int)$afstandInput;
        } else {
            print ("Ongeldige afstand, je reist nu standaard 10 kilometer");
        }

        $prijsNormaal = $afstand * $prijsPerKM;
        print ("De normale prijs zou " . $prijsNormaal . " geweest zijn.\n\n");

        $kaartInput = trimit("Wat voor kaarttype heb je? (week/weekend)");
        $kaart = strtolower($kaartInput);
        if ($kaart != 'week' && $kaart != 'weekend') {
            //ongeldige invoer, pas het aan naar een week kaart.
            $kaart = 'week';
        }

        $isFeestdagInput = trimit("Is het een feestdag? (ja/nee):");
        $isFeestdag = strtolower($isFeestdagInput == 'ja');

        if ($isFeestdag) {
            //het is een feestdag, bekijk de kaarten.
            print ("Jij hebt een " . $kaart . " kaart");
            $message = ", het is een feestdag ";

            $prijs = 0;

            if ($kaart == 'week') {
                $message = $message . "daarom krijg je nu 40% korting";
                $prijs = ($prijsNormaal * 0.60);
            } else {
                $message = $message . "daarom reis je nu gratis!";
            }
            print ($message);
            print ("\nDe eindprijs is dus: " . $prijs);
        } else {
            $isWeekendInput = trimit("Is het weekend? (ja/nee):");
            $isWeekend = strtolower($isWeekendInput == 'ja');

            $weekType = ["week" => false, "weekend" => true];
            print ("Jij hebt een " . $kaart . " kaart");
            foreach ($weekType as $type => $gratisWeekend) {
                if (strtolower($kaart) == strtolower($type)) {
                    $message = ", het is ";
                    $message = $message . ($isWeekend ? "weekend " : "doordeweeks ");
                    $output = $isWeekend ?
                        $message = $message . ($gratisWeekend ? "daarom reis je gratis!" : "daarom krijg je nu 40% korting.") :
                        $message = $message . ($gratisWeekend ? "daarom krijg je nu 40% korting" : "daarom reis je gratis!");

                    print ($message);
                }
                break;
            }
            print ("\nDe eindprijs is dus: " . adjustPrice($isWeekend, $gratisWeekend));
        }
    }

    function opdracht20()
    {
        $iemandJarig = FALSE;
        $aantalWekenZonderFeest = 3;
        $zomervakantie = FALSE;
        print ("\n");

        if ($iemandJarig || $zomervakantie || $aantalWekenZonderFeest) {
            print("Deze week is het feest!");
        } else {
            print("Deze week is er geen feest.");
        }
    }

    function opdracht21()
    {
        $aantalKoffie = 0;
        $koffieIsVers = FALSE;
        $laatInDeAvond = FALSE;
        if ($aantalKoffie == 0) {
            print("Ik neem koffie");
        } else {
            if ($laatInDeAvond || !$koffieIsVers || $aantalKoffie >= 5) {
                print("Ik neem geen koffie");
            } else {
                print("Ik neem koffie");
            }
        }
    }

    function opdracht22()
    {
        $hetIsOchtend = TRUE;
        $mamaBelt = TRUE;
        $ikSlaap = TRUE;
        print ("\n");
        if ($ikSlaap) {
            print ("Ik neem niet op");
        } else {
            if (!$hetIsOchtend) {
                print ("Ik neem op");
            } else if ($hetIsOchtend && $mamaBelt) {
                print ("Ik neem op");
            } else {
                print ("Ik neem niet op");
            }
        }
    }

    function opdracht23()
    {
    }

    function opdracht24()
    {
        $aantal = 8;
        $gebied = "onderwijs";
        if($aantal < 3) {
            print ("\nMark is boos");
        } else if($aantal >= 3 && $aantal <= 5) {
            print ("\nMark is humeurig");
        } else if($aantal > 5) {
            switch ($gebied) {
                case "onderwijs":
                    print ("\nMark is boos");
                    break;
                case "zorg":
                    print ("\nMark is verdrietig");
                    break;
                default:
                    print ("\nMark is blij");
            }
        }
    }

}